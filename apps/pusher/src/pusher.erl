-module(pusher).
-behaviour(gen_server).

%% API
-export([stop/1, start_link/0, send_message/2]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).
-record(state, {connection = undefined}).

stop(Name) ->
  gen_server:call(Name, stop).

start_link() ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

send_message(Device, Message) ->
  io:format("Calling sender for ~p: ~p~n", [Device, Message]),
  gen_server:call(?MODULE, {send, Device, Message}).

init(_Args) ->
  Connection = apns:connect(cert, my_connection),
  {ok, #state{connection = Connection}}.

handle_call({send, Device, Message}, _From, State) ->
  Notification = #{aps => #{alert => unicode:characters_to_binary(Message)}},
  Device1 = re:replace(Device, " ", "", [global, {return, binary}]),
  io:format("Push Notification ~p to ~p~n", [Notification, Device1]),
  apns:push_notification(my_connection, Device1, Notification),
  {reply, ok, State};
handle_call(stop, _From, State) ->
  {stop, normal, stopped, State};

handle_call(_Request, _From, State) ->
  {reply, ok, State}.

handle_cast(_Msg, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.





